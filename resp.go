package partner

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type resp struct {
	Payload any    `json:"payload"`
	Msg     string `json:"msg"`
	Code    uint   `json:"code"`
}

func WriteJsonError(c *gin.Context, code uint, msg string) {
	c.JSON(http.StatusOK, resp{
		Code: code,
		Msg:  msg,
	})
	c.Abort()
}

func WriteJsonSuccess(c *gin.Context, data any) {
	c.JSON(http.StatusOK, resp{
		Code:    200,
		Payload: data,
	})
}
