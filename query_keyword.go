package partner

type QueryKeyword struct {
	Page     string
	PageSize string
	OrderBy  string
	SoColumn string
	SoWord   string
}

var _defaultQueryKeyword = &QueryKeyword{
	Page:     "_page_",
	PageSize: "_page_size_",
	OrderBy:  "_order_by_",
	SoColumn: "_so_col_",
	SoWord:   "_so_word_",
}

func (kw *QueryKeyword) Words() []string {
	re := make([]string, 0, 5)
	re = append(re, kw.Page, kw.PageSize, kw.OrderBy, kw.SoColumn, kw.SoWord)
	return re
}
