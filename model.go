package partner

import "github.com/gin-gonic/gin"

type Model interface {
	GinHandlers() map[string]map[string][]gin.HandlerFunc
}
