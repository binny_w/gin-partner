package partner

import (
	"github.com/lionsoul2014/ip2region/binding/golang/xdb"
)

type IP2Region struct {
	bs *xdb.Searcher
}

func NewIP2Region(xdbFile string) (*IP2Region, error) {
	if bf, err := xdb.LoadContentFromFile(xdbFile); err != nil {
		return nil, err
	} else if bs, err := xdb.NewWithBuffer(bf); err != nil {
		return nil, err
	} else {
		return &IP2Region{bs: bs}, nil
	}
}

func (i *IP2Region) Search(ip string) (string, error) {
	return i.bs.SearchByStr(ip)
}

func (i *IP2Region) Close() {
	i.bs.Close()
}
