package ws

import (
	"github.com/gorilla/websocket"
	"net/http"
	"time"
)

type Message struct {
	Content    any      `json:"content,omitempty"`
	Sender     string   `json:"sender,omitempty"`
	Recipients []string `json:"recipients,omitempty"`
	Groups     []string `json:"groups,omitempty"`
}

type Client struct {
	Hub    *Hub
	Conn   *websocket.Conn
	Send   chan *Message
	Id     string
	Groups []string
}

type Hub struct {
	Clients    map[string]*Client
	Broadcast  chan *Message
	Register   chan *Client
	Unregister chan *Client
}

const (
	writeWait      = 10 * time.Second
	pongWait       = 60 * time.Second
	pingPeriod     = 55 * time.Second
	maxMessageSize = 2048
)

var _upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}
